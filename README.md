# Lakeshore Model 336 Cryogenic Temperature Controller

The Model 336 Cryogenic Temperature Controller is a device designed for temperature measurement and control specially on cryogenic ranges (click [here](https://www.lakeshore.com/products/categories/overview/temperature-products/cryogenic-temperature-controllers/model-336-cryogenic-temperature-controller) for further information on the product website).

The present repository is the EPICS IOC component that integrate this device on the EPICS environment. It was firstly designed firstly at the European Spallation Source (ESS) and applied on cryostats systems.
