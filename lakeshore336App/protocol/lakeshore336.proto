##################################################
#
# Protocol File
#
# Protocol file for Lakeshore 336
# Diamond Light Source, June 2010
#
# Oak Ridge National Lab, UT-Battelle, 2013-2014.
# Add more commands.
#
##################################################


##################################################
# General Settings
##################################################

Terminator = "\r\n";
ReplyTimeout = 1000;


###################################################
# Get functions
###################################################

# /// Read the temperature in celcius degree for inputs 1-4
getCRDG {
   out "CRDG? \$1";
   in "%f";
}

# /// Read the device firmware number
getFIRMWARE {
   in "LSCI,%*8c,%*15c,%s";
}

# /// Read the heater status for output 1 or 2
getHTR {
   out "HTR? \$1";
   in "%f";
}

# /// Read the device ID
getID {
   out "*IDN?";
   in "LSCI,%s";
}

# /// Read the device DISPLAY
# DISPLAY?
# 04,0,1
getDISPLAY {
   out "DISPLAY?";
   in "%(\$1)d,%(\$2)d,%(\$3)d";
}

# /// Read the input sensor name
getINNAME {
   out "INNAME? \$1";
   in "%#s";
}

# /// Read the input type parameter command
getINTYPE {
   out "INTYPE? \$1";
   in "%(\$2)d,%(\$3)d,%(\$4)d,%(\$5)d,%(\$6)d";
}

# /// Read the temperature in kelvin for inputs 1-4
getKRDG {
   out "KRDG? \$1";
   in "%f";
}

# /// Read the model number
getMODEL {
   in "LSCI,%8c,%*15c,%*s";
}

# /// Read the manual output value for outputs 1-4
getMOUT {
   out "MOUT? \$1";
   in "%f";
}

# /// Combine getOUTMODEMODE, getOUTMODEINPUT and getOUTMODEPE into one function.
# /// The first argument is the output number.
# /// The second argument is the output mode intput record.
# /// The third argument is the output mode powerup enable record.
getOM {
   out "OUTMODE? \$1";
   in "%d,%(\$2)d,%(\$3)d";
}

# /// Read the output mode input for outputs 1-4
# /// 0=None
# /// 1=A
# /// 2=B
# /// 3=C
# /// 4=D
getOUTMODEINPUT {
   out "OUTMODE? \$1";
   in "%*d,%d,%*d";
}

# /// Read the output mode mode status for outputs 1-4
# /// 0=Off
# /// 1=Closed Loop PID
# /// 2=Zone
# /// 3=Open Loop
# /// 4=Monitor Out
# /// 5=Warm Up Supply
getOUTMODEMODE {
   out "OUTMODE? \$1";
   in "%d,%*d,%*d";
}

# /// Read the output mode power up enable for outputs 1-4
# /// 0=Off
# /// 1=On
getOUTMODEPE {
   out "OUTMODE? \$1";
   in "%*d,%*d,%d";
}

# /// Read the PID params into 3 records using one write/read.
# /// The first argument is the output number. The second and third are
# /// the I and D records.
getPID {
   out "PID? \$1";
   in "%f,%(\$2)f,%(\$3)f";
}

# /// Read the ramp value and status for outputs 1-4
# /// The first parameter is the output number.
# /// The second parameter is the ramp status record.
getRAMP {
   out "RAMP? \$1";
   in "%(\$2)d,%f";
}

# /// Read the ramp status for outputs 1-4
getRAMPSTATUS {
   out "RAMP? \$1";
   in "%d,%*f";
}

# /// Read the range parameter (power range) for outputs 1-4
getRANGE {
   out "RANGE? \$1";
   in "%d";
}

# /// REad the status query
getRDGST {
   out "RDGST? \$1";
   in "%d";
}

# /// get the relay status
getRELAYST {
   out "RELAYST? \$1";
   in "%d";
}

# /// Read the serial number
getSERIAL {
   in "LSCI,%*8c,%15c,%*s";
}

# /// Read the setpoint for outputs 1-4
getSETP {
   out "SETP? \$1";
   in "%f";
}

# /// Read the voltage input for inputs 1-4
getSRDG {
   out "SRDG? \$1";
   in "%f";
}

# /// Read the tuning status
getTUNEST {
   out "TUNEST?";
   in "%s";
}

# /// Read the tuning status success param
getTUNESTSUCCESS {
   out "TUNEST?";
   in "%*d,%*d,%d,%*d";
}

# /// Read the ZONE parameters (this is read into a waveform)
getZONE {
   out "ZONE? \$1,\$2";
   separator=",";
   in "%f";
}

# /// Read the analog output for output 3 or 4
getAOUT {
   out "AOUT? \$1";
   in "%f";
}

# /// Read the output ANALOG settings
# 
# input:    ANALOG? <output>[term]
# return:   <input>,<units>,<high value>,<low value>,<polarity>[term]
getANALOG {
   out "ANALOG? \$1";
   in "%(\$2)d,%(\$3)d,%(\$4)f,%(\$5)f,%(\$6)d";
}

#######################################################
## Configure parameter for limiting Max output voltage (=100% power)
## on the Lake Shore 336
## RESET when LS336 is restarted!
## The description of the parameters are found online at http://forums.lakeshore.com/thread/61/limiting−power−analogue−output−shore
## Approximate formula CALG= <full range> / 20 CALZ= 0.5 − <full range>/40
## CALZ is afterwards optimized to give 0 volt out at 0% output.
#######################################################
getCALG {
   out "CALG? \$1, 0";
   in "%f";
}

getCALZ {
   out "CALZ? \$1, 0";
   in "%f";
}

#######################################################
# Set functions
#######################################################

# /// Start the auto tune process.
setATUNE {
   out "ATUNE \$1,%(\$2.VAL)d";
}

# Set the DISPLAY mode
setDISPLAY {
   out "DISPLAY %d";
}

# /// Set the PID D parameter for outputs 1-4
setD {
   out "PID \$3,%(\$1.VAL)f,%(\$2.VAL)f,%f";
   @init { out "PID? \$3"; in "%*f,%*f,%f"; }
}

# /// Set the ramp parameter for loops 1-2
#Need to pass in the PV name for the getRAMPSTATUS protocol.
setRAMP {
   out "RAMP \$2,%(\$1.VAL)d,%f";
   @init { out "RAMP? \$2"; in "%*d,%f"; }
}

# /// Set the ramp status for loops 1-2
#Need to pass in the PV name for the getRAMP protocol.
setRAMPSTATUS {
   out "RAMP \$2,%d,%(\$1.VAL)f";
   @init { out "RAMP? \$2"; in "%d,%*f"; }
}

# /// Set the PID I parameter for outputs 1-4
setI {
   out "PID \$2,%(\$1.VAL)f,%f,%(\$3.VAL)f";
   @init { out "PID? \$2"; in "%*f,%f,%*f"; }
}

# /// Set the input sensor name
setINNAME {
   out "INNAME \$1,\"%s\"";
   @init { getINNAME; }
}

# /// Set the input type parameter command
setINTYPE {
   out "INTYPE \$1,%(\$2)d,%(\$3)d,%(\$4)d,%(\$5)d,%(\$6)d";
   @init { getINTYPE; }
}

# /// Set the manual output value for outputs 1-4
setMOUT {
   out "MOUT \$1,%f";
   @init { getMOUT; }
}

# /// Set the output mode [loop],[mode],[input],[power up enable]
setOM {
   out "OUTMODE \$1,%d,%(\$2.VAL)d,%(\$3.VAL)d";
   @init { out "OUTMODE? \$1"; in "%d,%*d,%*d"; }
}

# /// Set the output mode input [loop],[mode],[input],[power up enable]
setOMI {
   out "OUTMODE \$1,%(\$2.VAL)d,%d,%(\$3.VAL)d";
   @init { out "OUTMODE? \$1"; in "%*d,%d,%*d"; }
}

# /// Set the output mode power up enable [loop],[mode],[input],[power up enable]
setOMP {
   out "OUTMODE \$1,%(\$2.VAL)d,%(\$3.VAL)d,%d";
   @init { out "OUTMODE? \$1"; in "%*d,%*d,%d"; }
}

# /// Set the PID P parameter for outputs 1-4
setP {
   out "PID \$1,%f,%(\$2.VAL)f,%(\$3.VAL)f";
   @init { out "PID? \$1"; in "%f,%*f,%*f"; }
}

# /// Set the range parameter for outputs 1-4
setRANGE {
   out "RANGE \$1,%d";
   @init { getRANGE; }
}

# /// Set the relay status
setRELAY {
   out "RELAY \$1, %d";
   @init { getRELAYST; }
}

# /// Set the setpoint for outputs 1-4
setSETP {
   out "SETP \$1,%f";
   @init { getSETP; }
}

# /// Set the ZONE parameters
setZONE {
   out "ZONE \$1,\$2,%(A)f,%(B)f,%(C)f,%(D)f,%(E)f,%(F)d,%(G)d,%(H)f";
}

# /// Set the output ANALOG settings
#
# ANALOG <output>,<input>,<units>,<high value>,<low value>,<polarity>[term]
#   <output>
#       Unpowered analog output to configure: 3 or 4
#   <input>
#       Specifies which input to monitor.
#           0 = none,
#           1 = Input A,
#           2 = Input B,
#           3 = Input C,
#           4 = Input D
#           (   5 = Input D2,
#               6 = Input D3,
#               7 = Input D4,
#               8 = Input D5 for 3062 option)
#   <units>
#       Specifies the units on which to base the output voltage:
#           1 = kelvin,
#           2 = Celsius,
#           3 = sensor units
#   <high value>
#       If output mode is Monitor Out, this parameter represents the data at which
#       the Monitor Out reaches +100% output. Entered in the units designated by
#       the <units> parmeter. Refer to OUTMODE command.
#   <low value>
#       If output mode is Monitor Out, this parameter represents the data at
#       which the analog output reaches -100% output if bipolar, or 0% output
#       if positive only. Entered in the units designated by the <units> parmeter.
#   <polarity>
#       Specifies output voltage is
#           0 = unipolar (positive output only) or
#           1 = bipolar (positive or negative output)
setANALOG {
   out "ANALOG \$1,%(\$2)d,%(\$3)d,%(\$4)f,%(\$5)f,%(\$6)d";
   @init { getANALOG; }
}

#######################################################
## Configure parameter for limiting Max output voltage (=100% power)
## on the Lake Shore 336
## RESET when LS336 is restarted!
## The description of the parameters are found online at http://forums.lakeshore.com/thread/61/limiting−power−analogue−output−shore
## Approximate formula CALG= <full range> / 20 CALZ= 0.5 − <full range>/40
## CALZ is afterwards optimized to give 0 volt out at 0% output.
#######################################################
setCALG {
   out "CALG \$1, 0, %f";
   @init { getCALG; }
}

setCALZ {
   out "CALZ \$1, 0, %f";
   @init { getCALZ; }
}